import React, {Component} from 'react';
import './styles/main.scss';
import Header from './components/header.js';
import Main from './components/main.js';
import Footer from './components/footer.js';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Main />
        <Footer />
      </div>
    )
  }
}

export default App;
 