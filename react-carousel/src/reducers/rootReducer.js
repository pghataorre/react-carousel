import initState from '../initialState/initState.js';

const rootReducer  = (state = initState, action) => {
  switch(action.type) {
    case 'FETCH_IMAGES':
      const galleryCount = action.galleryCollection[0].length + 2;

      return {
        ...state,
        galleryCount,
        galleryContent: action.galleryCollection[0],
        defaultContentMessage: action.apiErrorNoImages ? action.apiErrorNoImages : state.defaultContentMessage,
        galleryRange: {
          minRange: 0,
          maxRange: 4
        }
      };
    case 'FETCH_IMAGES_BACK': 
      return {
        ...state,
        galleryRange: {
          minRange: action.newMinRange,
          maxRange: action.newMaxRange
        }
      };
    case 'FETCH_IMAGES_FORWARD':
      return {
        ...state,
        galleryRange: {
          minRange: action.newMinRange,
          maxRange: action.newMaxRange
        }
      };
    default: 
      return state;
  }
}

export default rootReducer;