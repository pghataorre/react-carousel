import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import initState from './initialState/initState.js';
import rootReducer from './reducers/rootReducer.js';
import thunk from 'redux-thunk';

const middlewares = [thunk];
const store = createStore(rootReducer, initState, applyMiddleware(...middlewares));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
serviceWorker.unregister();