import React, {Component} from 'react';
import { connect } from 'react-redux';
import config from '../config.js';

class Inputsearch extends Component {
  searchImage = (event) => {
    const searchVal = event.target.elements[0].value;
    const searchValue = searchVal ? searchVal.trim().toLowerCase() : config.defaultSearch;
    let galleryCollection = [];
    const url = `${config.apiUrl}?key=${config.key}&q=${searchValue}&image_type=${config.defaultImageType}`;

    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        galleryCollection.push(response.hits);
        this.props.dispatchImageSearch(galleryCollection);
      })
      .catch((err) => {
        this.props.dispatchImageSearch(galleryCollection, config.notificationTextNoContent);
        console.log('ERROR')
      });

    event.preventDefault();
  }

  render() {
    return (
      <section className="form-search">
        <form method="post" onSubmit={this.searchImage}>
          <label htmlFor="search">Search for an image:</label>
          <input type="text" id="search" v-model="searchVal" placeholder="Add image type"/>
          <button type="submit"><span>Search Images</span></button>
          {this.hasContent}
        </form>
      </section>
    )
  }
}

const mapStateToProps = () => {
  return {};
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchImageSearch: (galleryCollection, apiErrorNoImages) => {
      dispatch({
        type: 'FETCH_IMAGES',
        galleryCollection,
        apiErrorNoImages
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Inputsearch);
 