import React from 'react';
import { connect } from 'react-redux';
const Carousellist = (props) => {

  const { minRange, maxRange } = props.galleryRange; 
  return (
    <section>
      <ol className="carousel__list">
        {
          props.galleryContent.map((item, index) => {
            if (index >= minRange && index <= maxRange) {
              let inlineStyle = {
                backgroundImage: `url(${item.webformatURL})`
              }

              return (
                <li style={inlineStyle} key={index}>
                  <p>{item.tags}</p>
                </li>
              )
            }
          })
        }
      </ol>
    </section>
  )
}

const mapStateToProps = (state) => {
  return {
    galleryContent: state.galleryContent,
    maxImages: state.maxImages,
    galleryRange: state.galleryRange
  }
}

export default connect(mapStateToProps)(Carousellist);
 