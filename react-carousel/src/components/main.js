import React, {Component} from 'react';
import Inputsearch from './inputSearch.js';
import Nocontent from './noContent.js';
import Carousellist from './carouselList.js';
import { connect } from 'react-redux';
import Defaultbutton from './defaultButton.js';

class Main extends Component {
  backWard = () => {
    let { minRange, maxRange } = this.props.galleryRange;
    if (minRange > 0) {
      minRange--;
      maxRange--;
      this.props.backWard(minRange, maxRange);
    }
  }

  forWard = () => {
    let { minRange, maxRange } = this.props.galleryRange;
    minRange++;
    maxRange++;
    this.props.forWard(minRange, maxRange);
  }

  render() {
    const {minRange, maxRange} = this.props.galleryRange;
    const {hasImages, galleryCount} = this.props;

    return (
      <main className="main-container">
        <Inputsearch />
        <section className="carousel">
          {hasImages && minRange > 0 ?
            (<Defaultbutton buttonEvent={this.backWard} classValues='carousel__button carousel__backward' buttonText='Back' />)
            : null
          }
          {hasImages ? (<Carousellist />) : (<Nocontent />)}
          {hasImages && maxRange <= galleryCount ? (<Defaultbutton buttonEvent={this.forWard} classValues='carousel__button carousel__forward' buttonText='Next' />) : null}
        </section>
      </main>
    )
  }
}

const checkHasImages = (state) => {
  return (Array.isArray(state.galleryContent)) && state.galleryContent.length !== 0;
}

const mapStateToProps = (state) => {
  return {
    hasImages: checkHasImages(state),
    galleryRange: state.galleryRange,
    galleryCount: state.galleryCount
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    backWard: (newMinRange, newMaxRange) => {
      dispatch({
        type: 'FETCH_IMAGES_BACK',
        newMinRange,
        newMaxRange
      });
    },
    forWard: (newMinRange, newMaxRange) => {
      dispatch({
        type: 'FETCH_IMAGES_FORWARD',
        newMinRange,
        newMaxRange
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
 