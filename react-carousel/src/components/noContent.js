import React from 'react';
import { connect } from 'react-redux';
const Nocontent = (props) =>  {
  return (
    <section>
      <p className="no-content">{props.defaultContentMessage}</p>
    </section>
  )
}

const mapStateToProps = (state) => {
  return {
    defaultContentMessage: state.defaultContentMessage
  }
}

export default connect(mapStateToProps)(Nocontent);
 