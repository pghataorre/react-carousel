import React from 'react';

const Defaultbutton = (props) => {
  return (
      <button 
        onClick={props.buttonEvent} 
        className={props.classValues}
        role="button" 
        tabIndex="0">
          <span className={props.isDesktop ? null : 'carousel__button--text'}>{props.buttonText}</span>
      </button>
  )
}

export default Defaultbutton;
 