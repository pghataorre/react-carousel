const initState = {
  defaultContentMessage: 'Please search for images.',
  galleryCount: 0,
  showOverlay: false,
  overlayImageUrl: '',
  maxImages: 4,
  galleryRange: {
    minRange: 0,
    maxRange: 4,
  },
  galleryContent: []
};

export default initState;